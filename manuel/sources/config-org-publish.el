;; configuration d'emacs pour compiler le manuel et l'exporter en html
(setq org-publish-project-alist
      '(("orgfiles"
         :base-directory "~/Documents/Boulo/SeedRoot/spim_git_inra/spim-piaf/manuel/sources/"
         :base-extension "org"
         :publishing-directory "../html/"
         :publishing-function org-html-publish-to-html
         ;;:exclude "PrivatePage.org" ;; regexp
         :headline-levels 3
         :section-numbers t
         :with-toc t
         :html-doctype "html5"
         :html-html5-fancy t
         :html-head "<link rel=\"stylesheet\"
                  href=\"./autres/manuel_style.css\" type=\"text/css\"/>"
         :html-preamble t)

        ("images"
         :base-directory "./images/"
         :base-extension "jpg\\|gif\\|png\\|svg"
         :publishing-directory "../html/images/"
         :recursive t
         :publishing-function org-publish-attachment)

        ("other"
         :base-directory "./autres/"
         :base-extension "css\\|el\\|js\\|pdf\\|fcstd\\|stl\\|dll\\|lib\\|h"
         :publishing-directory "../html/autres/"
         :recursive t
         :publishing-function org-publish-attachment)
        ("website" :components ("orgfiles" "images" "other")))
)

;; Pour les images dans org
;; Ajouter un
;; #+ATTR_ORG: :width 200
;; pour controler la taille
(setq org-image-actual-width nil)
